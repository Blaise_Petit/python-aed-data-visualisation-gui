# Description of the python project for the "Data Handling" module

## Initial remarks
### Group composition
This project has been made for educational purposes during the Data Handling module of MSc Business Analytics at Lancaster University. It is a group project made by: Haoran Zheng, Manjot Dhillon, Samuel Amelaine, Sizhuo Wang, Tom Krause, Yun-Yao Chiu and myself (Blaise Petit).

### Environment to run the code
You will need to have python 3 with following modules installed:
- plotly
- pandas
- numpy
- matplotlib
- seaborn
- PySimpleGUI

### Results
The score for this assignement was 79/100.

### French version
French version will be available below.

## Summary
We were given a .csv file about emergency arrival in hospitals in England. The system counts a breach everytime a patient stays longer than 4h in the hospital; the target of breaching have been set to 5% maximum. We provided a quick graphical analysis on a sample of 400 rows of these data, identifying problems and proposing solutions.
At the end of the code I create a simple GUI to randomly generate samples of the data, add/remove data according to conditions, etc...

Graphics and results are availabled and commented in the jupyter notebook file.

- - -

## Remarques initiales
### Composition du groupe
Ce projet universitaire a été réalisé dans le cadre du module "Data Handling" à l'université de Lancaster dans le cadre de la formation "Master of Science Business Analytics". Le projet a été réalisé en équipe avec: Haoran Zheng, Manjot Dhillon, Samuel Amelaine, Sizhuo Wang, Tom Krause et Yun-Yao Chiu et moi-même (Blaise Petit).

### Environnement pour exécuter le code
You will need to have python 3 with following modules installed:
- plotly
- pandas
- numpy
- matplotlib
- seaborn
- PySimpleGUI

### Résultat
Le devoir a été noté 79/100.

## Résumé
Nous avions à notre disposition un fichier .csv qui traitait des arrivées d'urgence dans les hopitaux anglais. D'après le système britanique, on considère comme défaillant un séjour qui dure plus de 4h pour un patient; l'objectif est d'avoir moins de 5% de taux de défaillance. Nous avons réalisé une analyse rapide graphique d'un échantillon de 400 lignes avant d'identifier les sources de problèmes et de proposer quelques solutions.
La dernière partie du code est consacrée à une interface graphique qui permet de générer des échantillons, ajouter/supprimer des données en fonction de conditions, etc... Cette partie n'a pas d'applications concrètes mais sert d'exercice de développement.
